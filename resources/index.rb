def install_rspec
  puts "Generating resources..."
  generate "rspec:install"

  copy_file "#{__dir__}/../files/.rspec", "spec/.rspec"
end

def setup_annotate
  puts "Setting up annotate..."
  generate "annotate:install"
  run "bundle exec annotate"
end

def setup_active_storage
  puts "Setting up active storage..."
  run "bin/rails active_storage:install"
end

def setup_action_text
  puts "Setting up action text..."
  run "bin/rails action_text:install"
end

def generate_migrations
  generate "migration add_name_to_users name:string admin:boolean"
end

def setup_authentication
  puts "Setting up authentication..."
  generate "authentication --pwned --sudoable --omniauthable --two-factor --trackable --lockable --code-verifiable && bundle install"

  gsub_file "Gemfile", 'gem "authentication-zero"', "# gem 'authentication-zero'"
  run "bin/setup"
end

def generate_models
  generate "model notification recipient:references{polymorphic} read_at:datetime interacted_at:datetime type:string params:jsonb"
  inject_into_file "app/models/user.rb", after: "class User < ApplicationRecord" do
    <<-'RUBY'

    has_many :notifications, as: :recipient, dependent: :destroy
    has_one_attached :avatar
    RUBY
  end

  copy_file "#{__dir__}/../files/models/notification.rb", "app/models/notification.rb", force: true
end

def setup_administrate
  puts "Setting up administrate..."
  generate "administrate:install"
end

def setup_home_page
  generate :controller, "home index"
  route "root to: 'home#index'"
end
