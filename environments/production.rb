def update_production_rb
  puts "Updating config/environments/production.rb"
  application(nil, env: "production") do
    "config.active_storage.service = :digitalocean_spaces"
  end
end
