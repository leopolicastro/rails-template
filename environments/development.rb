def update_development_rb
  puts "Updating config/environments/development.rb"
  # rubocop:disable Lint/Void
  application(nil, env: "development") do
    "config.action_mailer.default_url_options = {host: 'localhost', port: 3000}"
    "config.action_mailer.delivery_method = :letter_opener"
    "config.action_mailer.perform_deliveries = true"
    "config.active_storage.service = :digitalocean_spaces"
  end
  # rubocop:enable Lint/Void
end
