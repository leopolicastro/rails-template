require_relative "development"
require_relative "production"

def update_routes
  puts "Updating config/routes.rb"
  route "resources :people"
  route "resources :notifications, only: [:index, :show]"

  inject_into_file "config/routes.rb", after: "resources :notifications, only: [:index, :show]\n" do
    <<~RUBY
      if defined?(Sidekiq)
        require "sidekiq/web"
        mount Sidekiq::Web => "/sidekiq"
      end
      mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

    RUBY
  end
end

def setup_tailwind_stimulus_components
  run "bin/importmap pin tailwindcss-stimulus-components"

  inject_into_file "app/javascript/controllers/application.js", after: "window.Stimulus   = application\n" do
    <<~JS

      import {
        Alert,
        Autosave,
        Dropdown,
        Modal,
        Tabs,
        Popover,
        Toggle,
        Slideover,
      } from "tailwindcss-stimulus-components";

      application.register("alert", Alert);
      application.register("autosave", Autosave);
      application.register("dropdown", Dropdown);
      application.register("modal", Modal);
      application.register("tabs", Tabs);
      application.register("popover", Popover);
      application.register("toggle", Toggle);
      application.register("slideover", Slideover);
    JS
  end
end
