User.create!(
  name: "Admin User",
  email: "admin@example.com",
  password: "Abc123Abc123!",
  password_confirmation: "Abc123Abc123!",
  admin: true,
  verified: true
)

User.create!(
  name: "Some User",
  email: "some@example.com",
  password: "Abc123Abc123!",
  password_confirmation: "Abc123Abc123!",
  admin: false,
  verified: true
)

User.all.each do |user|
  user.update(verified: true)
end

puts "#{User.count} users created"
