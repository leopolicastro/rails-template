module NotificationsHelper
  def unread_notifications_count
    current_user.notifications.unread.count
  end

  def unread_notifications?
    current_user.notifications.unread.any?
  end

  def unread_class
    "border border-indigo-700 rounded-full shadow shadow-indigo-500" if unread_notifications?
  end
end
