# Post Bundle
def copy_config_files
  puts "Copying files..."
  copy_file "#{__dir__}/render.yaml", "render.yml"
  copy_file "#{__dir__}/overcommit.yml", ".overcommit.yml"
  copy_file "#{__dir__}/rubocop.yml", ".rubocop.yml"
  copy_file "#{__dir__}/render.yaml", ".render.yaml"
end

def copy_templates
  puts "Copying templates..."
  copy_file "#{__dir__}/lib/templates/active_record/model/model.rb.tt", "lib/templates/active_record/model/model.rb.tt"
  copy_file "#{__dir__}/lib/templates/noticed/notification/notification.rb.tt", "lib/templates/noticed/notification/notification.rb.tt"
  copy_file "#{__dir__}/lib/templates/pundit/install/application_policy.rb.tt", "lib/templates/pundit/install/application_policy.rb.tt"
  copy_file "#{__dir__}/lib/templates/pundit/policy/policy.rb.tt", "lib/templates/pundit/policy/policy.rb.tt"
  copy_file "#{__dir__}/lib/templates/rails/scaffold_controller/controller.rb.tt", "lib/templates/rails/scaffold_controller/controller.rb.tt"
end

def copy_controllers
  puts "Copying controllers..."
  directory "#{__dir__}/controllers", "app/controllers"
end

def copy_helpers
  puts "Copying helpers..."
  directory "#{__dir__}/helpers", "app/helpers"
end

def define_current_user
  inject_into_file "app/controllers/application_controller.rb", after: "before_action :authenticate" do
    <<-'RUBY'

    def current_user
      Current.user
    end
    helper_method :current_user

    RUBY
  end
end

def include_gem_modules
  inject_into_file "app/controllers/application_controller.rb", after: "class ApplicationController < ActionController::Base" do
    <<-'RUBY'

    include Pagy::Backend
    include Pundit::Authorization

    RUBY
  end
end

def copy_seeds
  directory "#{__dir__}/seeds", "db/seeds"
  append_file "db/seeds.rb" do
    <<~'RUBY'
      def seed(file)
        load Rails.root.join("db", "seeds", "#{file}.rb")
      end

      puts "Seeding #{Rails.env} database"

      seed :users

      puts "Done!"

    RUBY
  end
end

def copy_views
  # Loop throw all the folders in the files/views folder and copy them
  # to the views folder
  Dir.glob("#{__dir__}/views/*").each do |folder|
    directory folder, "app/views/#{File.basename(folder)}", force: true
  end
end

def update_storage_file
  append_file "config/storage.yml" do
    <<~'YAML'
      digitalocean_spaces:
        service: S3
        access_key_id: <%= Rails.application.credentials.dig(:digitalocean_spaces, :access_key_id) %>
        secret_access_key: <%= Rails.application.credentials.dig(:digitalocean_spaces, :secret_access_key) %>
        region: <%= Rails.application.credentials.dig(:digitalocean_spaces, :region) %>
        endpoint: <%= Rails.application.credentials.dig(:digitalocean_spaces, :endpoint) %>
        bucket: <%= Rails.application.credentials.dig(:digitalocean_spaces, :bucket) %>
    YAML
  end
end

# Pre Bundle
def update_app_layout
  inject_into_file "app/views/layouts/application.html.erb", after: "<body>" do
    <<-'ERB'

    <%= render "shared/navbar" %>
    <%= render "shared/alerts" %>
    ERB
  end
  inject_into_file "app/views/layouts/application.html.erb", before: "</body>" do
    <<-'ERB'

    <%= render "shared/pagination" %>
    <%#= render "shared/footer" %>
    ERB
  end
end

def update_app_helper
  inject_into_file "app/helpers/application_helper.rb", after: "module ApplicationHelper" do
    <<-'RUBY'

    include Pagy::Frontend

    RUBY
  end
end

def copy_images
  copy_file "#{__dir__}/assets/images/avatar.png", "app/assets/images/avatar.png"
end

def update_css
  append_file "app/assets/stylesheets/application.css" do
    <<~'CSS'

        html,
      body {
        height: 100%;
      }
      main {
        min-height: 95%;
        margin-bottom: -160px;
      }
      main:after {
        content: "";
        display: block;
        height: 100px;
      }

      .active {
        border-bottom: 1px solid black;
      }

      .active-sidebar {
        background-color: #4337c9;
        color: white;
      }
    CSS
  end
end
