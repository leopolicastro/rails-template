require "rails/generators/active_record"

require_relative "gems/index"
require_relative "initializers/index"
require_relative "environments/index"
require_relative "files/index"
require_relative "resources/index"

add_gems
add_initializers
update_development_rb
update_production_rb
update_app_layout
update_app_helper

after_bundle do
  install_rspec
  setup_active_storage
  setup_action_text
  setup_home_page
  setup_authentication
  generate_migrations
  setup_annotate
  generate_models
  setup_administrate
  copy_config_files
  copy_templates
  copy_controllers
  copy_helpers
  define_current_user
  include_gem_modules
  copy_seeds
  copy_views
  copy_images
  update_routes
  setup_tailwind_stimulus_components
  update_css
  setup_pagy
  run "bin/setup"
  git add: "."
  run "git commit -m 'First commit!'"
  rake "db:seed"
  puts IO.read(File.expand_path("bender.txt", __dir__))
  readme "README.md"
end
