def production_gems
  gem "active_link_to"
  gem "administrate"
  gem "administrate-field-active_storage"
  gem "authentication-zero"
  gem "aws-sdk-s3", require: false
  gem "high_voltage"
  gem "honeybadger", "~> 5.0"
  gem "image_processing", "~> 1.2"
  gem "noticed"
  gem "oj"
  gem "pagy"
  gem "pundit"
  gem "sidekiq"
end
