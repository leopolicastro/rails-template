require_relative "./development"
require_relative "./production"
require_relative "./test"

def add_gems
  development_gems
  test_gems
  production_gems
end
