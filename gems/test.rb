def test_gems
  gem_group :development, :test do
    gem "annotate"
    gem "capybara"
    gem "coderay"
    gem "factory_bot_rails"
    gem "faker"
    gem "rspec-rails"
    gem "shoulda-matchers"
    gem "webdrivers"
  end
end
