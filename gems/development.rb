def development_gems
  gem_group :development do
    gem "erb_lint", require: false
    gem "letter_opener_web", "~> 2.0"
    gem "standard", require: false
  end
end
