# README

## Description

## Requirements

## Setup

Run `rails credentials:edit --environment=development` and add the following with your own values:

```yml
digitalocean_spaces:
  access_key_id:
  secret_access_key:
  region:
  endpoint: https://nyc3.digitaloceanspaces.com
  bucket:

secret_key_base: 7f2a1c6b18d719cdc34dc910729e36624677dadffdb3ba42116a946e667745930967791b02a999bccdad7b72287b3fbdaba7e651ae5138daeee6ef195af772cc

active_record_encryption:
  primary_key: wQWpGD9PTEQulZRqn1tUBfhVC9MiefF2
  deterministic_key: e4rrDBL5UNzv7wqhfThBO3XG7E4Udsjf
  key_derivation_salt: tFKE1kpMnusCFbgkNSw5OWrs0Asojnso

honeybadger:
  api_key: hbp_update
```

## Usage
